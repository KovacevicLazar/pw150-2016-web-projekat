﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFirstMVCWebApp.Controllers
{
    public class GostController : Controller
    {
        // GET: Gost
        public ActionResult Index(FormCollection form, string TIP ,string Grad, string CenaOd,string CenaDo,string BrojOsoba,string BrojSoba)
        {
            List<Apartman> apartmani = new List<Apartman>();
            foreach (var apart in ApartmaniISadrzaji.Apartmani.Values)
            {
                if (apart.StatusApartmana == Enumeracije.StatusApartmana.AKTIVAN && apart.Obrisan==false)
                {
                   apartmani.Add(apart);
                }
            }


            if (Request["dugme"] == "sortiraj")
            {
                apartmani = sortiranje(apartmani, Request["SORTIRANJE"]);
            }
            else if (Request["dugme"] == "Primeni filter")
            {
                if (!string.IsNullOrEmpty(form["Sadrzaji"]))
                {
                    apartmani = filtrirajPoSadrzaji(apartmani, form["Sadrzaji"]);
                }
                if (TIP != null && TIP != "SVI")
                {
                    Enumeracije.TipApart tip;
                    if (TIP == "CEO_APART")
                    {
                        tip = Enumeracije.TipApart.CEO_APART;
                    }
                    else
                    {
                        tip = Enumeracije.TipApart.SOBA;
                    }
                    List<Apartman> apartmaniPom = new List<Apartman>();
                    foreach (var apart in apartmani)
                    {
                        if (apart.TipApartmana == tip)
                        {
                            apartmaniPom.Add(apart);
                        }
                    }
                    apartmani = apartmaniPom;
                }
            }
            else if (Request["dugme"] == "Pretrazi")
            {
                apartmani = pretraga(apartmani, Grad, BrojOsoba, BrojSoba, CenaOd, CenaDo);
            }
            ViewBag.ListaSadrzaja = GetSadrzaj(null);
            return View(apartmani);
        }

        private MultiSelectList GetSadrzaj(string[] OznaceniSadrzaj)
        {
            List<Sadrzaj> Sadrzaji = new List<Sadrzaj>();
            foreach (var sadrzaj in ApartmaniISadrzaji.Sadrzaji.Values)
            {
                if (sadrzaj.Obrisan == false)
                {
                    Sadrzaji.Add(sadrzaj);
                }
            }
            return new MultiSelectList(Sadrzaji, "ID", "Naziv", OznaceniSadrzaj);
        }

        public ActionResult PrikazProfila()
        {
            HttpCookie cookie = Request.Cookies["Korisnik"];
            var gost =Gosti.gosti[cookie.Value];
            return View(gost);
        }


        [HttpPost]
        public ActionResult AzuriranjeProfila(string korime, string lozinka, string ime, string prezime, string pol)
        {
            if (string.IsNullOrEmpty(korime) || string.IsNullOrEmpty(lozinka) || string.IsNullOrEmpty(ime) || string.IsNullOrEmpty(prezime) || string.IsNullOrEmpty(pol))
            {
                Session["Poruka"] = "Sva polja moraju biti popunjena.";
                return RedirectToAction("PrikazProfila");
            }

            HttpCookie cookie = Request.Cookies["Korisnik"];
            var gost = Gosti.gosti[cookie.Value];
            Enumeracije.Pol Pol;
            if (pol == Enumeracije.Pol.MUSKO.ToString())
            {
                Pol = Enumeracije.Pol.MUSKO;
            }
            else
            {
                Pol = Enumeracije.Pol.ZENSKO;
            }

            if (gost.KorisnickoIme != korime) //ako je menjao kor ime
            {
                if (!Administratori.aministratori.ContainsKey(korime) && !Domacini.domacini.ContainsKey(korime) && !Gosti.gosti.ContainsKey(korime)) //ako novo kor ime niko drugi vec ne koristi
                {
                    string staroIme = gost.KorisnickoIme;
                    Gosti.gosti[staroIme].KorisnickoIme = korime;
                    Gosti.gosti[staroIme].Lozinka = lozinka;
                    Gosti.gosti[staroIme].Ime = ime;
                    Gosti.gosti[staroIme].Prezime = prezime;
                    Gosti.gosti[staroIme].Pol = Pol;

                    HttpCookie GostCookie = new HttpCookie("Korisnik", korime);
                    HttpContext.Response.SetCookie(GostCookie);

                    Gosti.gosti[korime] = Gosti.gosti[staroIme];
                    Gosti.gosti.Remove(staroIme);


                    IzmeniTxt(Gosti.gosti[korime], "gosti.txt",staroIme);
                    Session["Poruka"] = "Podaci su promenjeni uspesno.";
                    return RedirectToAction("PrikazProfila");
                }
                else
                {
                    Session["Poruka"] = "Ovo korisnicko ime je zauzeto.";
                    return RedirectToAction("PrikazProfila");
                }
            }
            else //Ako nije menjao kor ime
            {
                Gosti.gosti[gost.KorisnickoIme].KorisnickoIme = korime;
                Gosti.gosti[gost.KorisnickoIme].Lozinka = lozinka;
                Gosti.gosti[gost.KorisnickoIme].Ime = ime;
                Gosti.gosti[gost.KorisnickoIme].Prezime = prezime;
                Gosti.gosti[gost.KorisnickoIme].Pol = Pol;
                IzmeniTxt(Gosti.gosti[gost.KorisnickoIme], "gosti.txt",korime);
                Session["Poruka"] = "Podaci su promenjeni uspesno.";
                return RedirectToAction("PrikazProfila");
            }
        }

        private void IzmeniTxt(Korisnik k, string fajl,string staroIme)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            string[] lines = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains(staroIme))
                {
                    string novi = k.KorisnickoIme + '|' + k.Lozinka + '|' + k.Ime + '|' + k.Prezime + '|' + k.Pol;
                    lines[i] = novi;
                }
            }
            System.IO.File.WriteAllLines(path, lines);
        }


        private List<Apartman> sortiranje(List<Apartman> apartmani, string redosled)
        {
            if (redosled == "opadajuce")
            {
                Apartman temp = new Apartman();
                for (int i = 0; i < apartmani.Count - 1; i++)
                {
                    for (int j = i + 1; j < apartmani.Count; j++)
                    {
                        if (apartmani[i].Cena < apartmani[j].Cena)
                        {
                            temp = apartmani[j];
                            apartmani[j] = apartmani[i];
                            apartmani[i] = temp;
                        }
                    }
                }
            }
            else if (redosled == "rastuce")
            {
                Apartman temp = new Apartman();
                for (int i = 0; i < apartmani.Count - 1; i++)
                {
                    for (int j = i + 1; j < apartmani.Count; j++)
                    {
                        if (apartmani[i].Cena > apartmani[j].Cena)
                        {
                            temp = apartmani[j];
                            apartmani[j] = apartmani[i];
                            apartmani[i] = temp;
                        }
                    }
                }
            }
            return apartmani;
        }

        private List<Apartman> pretraga(List<Apartman> apartmani, string Grad, string BrojOsoba, string BrojSoba, string CenaOd, string CenaDo)
        {
            List<Apartman> apartmaniPom = new List<Apartman>();
            foreach (var apartman in apartmani)
            {
                if (!string.IsNullOrEmpty(Grad))
                {
                    if (!(apartman.Lokacija.Adresa.NaseljenoMesto == Grad))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(BrojOsoba))
                {
                    if (!(apartman.BrojGostiju == int.Parse(BrojOsoba)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(BrojSoba))
                {
                    if (!(apartman.BrojSoba == int.Parse(BrojSoba)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(CenaOd))
                {
                    if ((apartman.Cena < int.Parse(CenaOd)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(CenaDo))
                {
                    if ((apartman.Cena > int.Parse(CenaDo)))
                    {
                        continue;
                    }
                }
                apartmaniPom.Add(apartman);
            }
            return apartmaniPom;
        }

        private List<Apartman> filtrirajPoSadrzaji(List<Apartman> apartmani, string sadrzaji)
        {
            string sviID = sadrzaji;
            string[] SadrzajiID = sviID.Split(',');
            List<Apartman> apartmaniPom = new List<Apartman>();
            if (SadrzajiID.Count<string>() == 1)
            {
                foreach (var apart in apartmani)
                {
                    foreach (var sadrzaj in apart.SadrzajApartmana)
                    {
                        foreach (string ID in SadrzajiID)
                        {
                            if (sadrzaj.Naziv == ApartmaniISadrzaji.Sadrzaji[int.Parse(ID)].Naziv)
                            {
                                apartmaniPom.Add(apart);
                            }
                        }
                    }
                }
            }
            else //Oko je korisnik izabrao vise sadrzaja..pa trazimo samo apartmane koji sadrze sve to
            {
                foreach (var apart in apartmani)
                {
                    bool nasaoprethodni = true;
                    bool nadjeniSvi = true;
                    foreach (string ID in SadrzajiID)
                    {
                        if (nasaoprethodni)
                        {
                            nasaoprethodni = false;
                            foreach (var sadrzaj in apart.SadrzajApartmana)
                            {

                                if (sadrzaj.Naziv == ApartmaniISadrzaji.Sadrzaji[int.Parse(ID)].Naziv)
                                {
                                    nasaoprethodni = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            nadjeniSvi = false;
                            break;
                        }
                    }
                    if (nadjeniSvi && nasaoprethodni) //drugi uslov hvata slucaj ako zadna stavka nije nadjena
                    {
                        apartmaniPom.Add(apart);
                    }
                }
            }
            return apartmaniPom;
        }

    }
}