﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFirstMVCWebApp.Controllers
{
    public class DomacinController : Controller
    {
        // GET: Domacin
        
        public ActionResult Index(FormCollection form,string STATUS,string TIP, string Grad, string CenaOd, string CenaDo, string BrojOsoba, string BrojSoba)
        {
            HttpCookie cookie = Request.Cookies["Korisnik"];
            var domacin = Domacini.domacini[cookie.Value];

            List<Apartman> apartmani = new List<Apartman>();
            foreach (var apart in Domacini.domacini[domacin.KorisnickoIme].ApartmaniZaIzdavanje.Values)
            {
                if (apart.Obrisan == false)
                {
                    apartmani.Add(apart);
                }
            }

            if (Request["dugme"] == "sortiraj")
            {
                apartmani = sortiranje(apartmani, Request["SORTIRANJE"]);
            }
            else if (Request["dugme"] == "Primeni filter")
            {
                if (!string.IsNullOrEmpty(form["Sadrzaji"]))
                {
                    apartmani = filtrirajPoSadrzaji(apartmani, form["Sadrzaji"]);
                }
                if (STATUS != null && STATUS != "SVI")
                {
                    Enumeracije.StatusApartmana status;
                    if (STATUS == "AKTIVAN")
                    {
                        status = Enumeracije.StatusApartmana.AKTIVAN;
                    }
                    else
                    {
                        status = Enumeracije.StatusApartmana.NEAKTIVAN;
                    }
                    List<Apartman> apartmaniPom = new List<Apartman>();
                    foreach (var apart in apartmani)
                    {
                        if (apart.StatusApartmana == status)
                        {
                            apartmaniPom.Add(apart);
                        }
                    }
                    apartmani = apartmaniPom;
                }
                if (TIP != null && TIP != "SVI")
                {
                    Enumeracije.TipApart tip;
                    if (TIP == "CEO_APART")
                    {
                        tip = Enumeracije.TipApart.CEO_APART;
                    }
                    else
                    {
                        tip = Enumeracije.TipApart.SOBA;
                    }
                    List<Apartman> apartmaniPom = new List<Apartman>();
                    foreach (var apart in apartmani)
                    {
                        if (apart.TipApartmana == tip)
                        {
                            apartmaniPom.Add(apart);
                        }
                    }
                    apartmani = apartmaniPom;
                }
            }
            else if (Request["dugme"] == "Pretrazi")
            {
                apartmani = pretraga(apartmani, Grad, BrojOsoba, BrojSoba, CenaOd, CenaDo);
            }

            ViewBag.ListaSadrzaja = GetSadrzaj(null);

            return View(apartmani);

        }

        public ActionResult PrikazProfila()
        {
            HttpCookie cookie = Request.Cookies["Korisnik"];
            var domacin = Domacini.domacini[cookie.Value];
            return View(domacin);
        }

        [HttpPost]
        public ActionResult AzuriranjeProfila(string korime, string lozinka, string ime, string prezime, string pol)
        {
            if (string.IsNullOrEmpty(korime)||string.IsNullOrEmpty(lozinka)||string.IsNullOrEmpty(ime)||string.IsNullOrEmpty(prezime)||string.IsNullOrEmpty(pol)){
                Session["Poruka"] = "Sva polja moraju biti popunjena.";
                return RedirectToAction("PrikazProfila");
            }
            HttpCookie cookie = Request.Cookies["Korisnik"];
            var domacin = Domacini.domacini[cookie.Value];
            Enumeracije.Pol Pol;
            if (pol == Enumeracije.Pol.MUSKO.ToString())
            {
                Pol = Enumeracije.Pol.MUSKO;
            }
            else
            {
                Pol = Enumeracije.Pol.ZENSKO;
            }

            if (domacin.KorisnickoIme != korime) //ako je menjao kor ime
            {
                if (!Administratori.aministratori.ContainsKey(korime) && !Domacini.domacini.ContainsKey(korime) && !Gosti.gosti.ContainsKey(korime)) //ako novo kor ime niko drugi vec ne koristi
                {
                    string staroIme = domacin.KorisnickoIme;
                    Domacini.domacini[staroIme].KorisnickoIme = korime;
                    Domacini.domacini[staroIme].Lozinka = lozinka;
                    Domacini.domacini[staroIme].Ime = ime;
                    Domacini.domacini[staroIme].Prezime = prezime;
                    Domacini.domacini[staroIme].Pol = Pol;
                    
                    HttpCookie DomacinCookie = new HttpCookie("Korisnik", korime);
                    HttpContext.Response.SetCookie(DomacinCookie);

                    Domacini.domacini[korime] = Domacini.domacini[staroIme];
                    Domacini.domacini.Remove(staroIme);

                    IzmeniKorisnikaTxt(Domacini.domacini[korime], "domacini.txt",staroIme);
                    Session["Poruka"] = "Podaci su promenjeni uspesno.";
                }
                else
                {
                    Session["Poruka"] = "Ovo korisnicko ime je zauzeto.";
                    
                }
            }
            else //Ako nije menjao kor ime
            {
                Domacini.domacini[domacin.KorisnickoIme].KorisnickoIme = korime; //moze bez ovoga
                Domacini.domacini[domacin.KorisnickoIme].Lozinka = lozinka;
                Domacini.domacini[domacin.KorisnickoIme].Ime = ime;
                Domacini.domacini[domacin.KorisnickoIme].Prezime = prezime;
                Domacini.domacini[domacin.KorisnickoIme].Pol = Pol;
                IzmeniKorisnikaTxt(Domacini.domacini[korime], "domacini.txt",korime);
                Session["Poruka"] = "Podaci su promenjeni uspesno.";
            }
            foreach(var apartman in Domacini.domacini[domacin.KorisnickoIme].ApartmaniZaIzdavanje.Values) //Mora se azirirati i domacin apartmana
            {
                apartman.Domacin = Domacini.domacini[domacin.KorisnickoIme];
            }
            return RedirectToAction("PrikazProfila");
        }

        
        private MultiSelectList GetSadrzaj(string[] OznaceniSadrzaj)
        {
            List<Sadrzaj> Sadrzaji = new List<Sadrzaj>();
            foreach(var sadrzaj in ApartmaniISadrzaji.Sadrzaji.Values)
            {
                if (sadrzaj.Obrisan == false)
                {
                    Sadrzaji.Add(sadrzaj);
                }
            }
            return new MultiSelectList(Sadrzaji, "ID", "Naziv", OznaceniSadrzaj);
        }

        public ActionResult DodajApartman()
        {
            ViewBag.ListaSadrzaja = GetSadrzaj(null);
            return View();
        }

        [HttpPost]
        public ActionResult DodajApartman(FormCollection form,string DatumOd,string DatumDo,string Postanski, string GeoSirina,string GeoDuzina,string TipApa, string brojSoba,string brojGostiju, string mesto, string ulica, string broj, string cena)
        {
            if (string.IsNullOrEmpty(DatumDo) || string.IsNullOrEmpty(DatumOd) || string.IsNullOrEmpty(Postanski) || string.IsNullOrEmpty(GeoDuzina) || string.IsNullOrEmpty(GeoSirina) || string.IsNullOrEmpty(TipApa) || string.IsNullOrEmpty(brojSoba) || string.IsNullOrEmpty(brojGostiju) || string.IsNullOrEmpty(mesto) || string.IsNullOrEmpty(ulica) || string.IsNullOrEmpty(broj) || string.IsNullOrEmpty(cena))
            {
                Session["PorukaDodavanje"] = "Sva polja moraju biti popunjena.";
                return RedirectToAction("DodajApartman");
            }
            Session["PorukaDodavanje"] = "";

            HttpCookie cookie = Request.Cookies["Korisnik"];
            var domacin = Domacini.domacini[cookie.Value];

            DateTime Datumod = DateTime.Parse(DatumOd);
            DateTime Datumdo = DateTime.Parse(DatumDo);

            List<DateTime> datumi = new List<DateTime>();
            DateTime datum = new DateTime(Datumod.Year, Datumod.Month, Datumod.Day);
            while (datum != Datumdo)
            {
                datumi.Add(datum);
                datum= datum.AddDays(1);
            }

            Enumeracije.TipApart tip;
            if (Enumeracije.TipApart.CEO_APART.ToString() == TipApa)
            {
                tip = Enumeracije.TipApart.CEO_APART;
            }
            else
            {
                tip = Enumeracije.TipApart.SOBA;
            }

            Apartman apartman = new Apartman(domacin, tip, int.Parse(brojSoba), int.Parse(brojGostiju), new Lokacija(GeoSirina, GeoDuzina, new Adresa(ulica, int.Parse(broj), int.Parse(Postanski), mesto)), int.Parse(cena));

            List<Slika> list = new List<Slika>();

            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Images/"), fileName);

                        file.SaveAs(path);
                        Slika slika = new Slika(fileName, path);
                        UpisApartmanSlika(apartman.ID, slika.ID, "SlikaID_ApartmanID.txt");
                        list.Add(slika);
                        UpisSlike(slika, "slike.txt");
                    }
                }

            }
          
            if (!string.IsNullOrEmpty(form["Sadrzaji"]))
            {
                string OznaceniSadrzaj = form["Sadrzaji"];
                string[] SadrzajiID = OznaceniSadrzaj.Split(',');
                foreach (string id in SadrzajiID)
                {
                    apartman.SadrzajApartmana.Add(ApartmaniISadrzaji.Sadrzaji[int.Parse(id)]);
                    UpisApartmanSadrzaj(apartman.ID, id, "apartmanID_SadrzajID.txt");
                }
            }
            foreach(var slika in list)
            {
                apartman.Slike.Add(slika);
            }
            apartman.DatumizaIzdavanje = datumi;
            apartman.DostupniDatumi = datumi;
            Domacini.domacini[domacin.KorisnickoIme].ApartmaniZaIzdavanje.Add(apartman.ID, apartman);
            ApartmaniISadrzaji.Apartmani.Add(apartman.ID, apartman);
            UpisApartmana(apartman, "apartmani.txt");
            
            return RedirectToAction("Index");
        }

        
        public ActionResult IzmeniIzbrisi(int id)
        {
            HttpCookie cookie = Request.Cookies["Korisnik"];
            var domacin = Domacini.domacini[cookie.Value];

            if (Request["izmena"] == "Modifikuj")
            {
                ViewBag.ListaSadrzaja = GetSadrzaj(null);
                return View(ApartmaniISadrzaji.Apartmani[id]);
            }
            else
            {
                ApartmaniISadrzaji.Apartmani[id].Obrisan = true;
                Domacini.domacini[domacin.KorisnickoIme].ApartmaniZaIzdavanje[id].Obrisan = true;
                UpisIzmeneApartmana(ApartmaniISadrzaji.Apartmani[id], "apartmani.txt");
                //ApartmaniISadrzaji.Apartmani.Remove(id);
                return RedirectToAction("Index");
            }
        }
        
        [HttpPost]
        public ActionResult IzmenaApartmana(FormCollection form, string Postanski, string GeoSirina, string GeoDuzina, int ID,string TipApa, string brojSoba, string brojGostiju, string mesto, string ulica, string broj, string cena)
        {
            HttpCookie cookie = Request.Cookies["Korisnik"];
            var domacin = Domacini.domacini[cookie.Value];

            Enumeracije.TipApart tip;
            if (Enumeracije.TipApart.CEO_APART.ToString() == TipApa)
            {
                tip = Enumeracije.TipApart.CEO_APART;
            }
            else
            {
                tip = Enumeracije.TipApart.SOBA;
            }

            if (Request.Files.Count > 0)
            {
                bool stareIzbrisane = false;
               
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    if (file != null && file.ContentLength > 0)
                    {
                        if (!stareIzbrisane)
                        {
                            BrisanjeStarihSlika(ID, "SlikaID_ApartmanID.txt");
                            ApartmaniISadrzaji.Apartmani[ID].Slike.Clear();
                            stareIzbrisane = true;
                        }
                        
                          //Ako su dodate nove..stare ukloni
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Images/"), fileName);

                        file.SaveAs(path);
                        Slika slika = new Slika(fileName, path);
                        ApartmaniISadrzaji.Apartmani[ID].Slike.Add(slika);
                        UpisSlike(slika, "slike.txt");
                        UpisApartmanSlika(ID, slika.ID, "SlikaID_ApartmanID.txt");
                    } 
                }
            }
           
            Lokacija lokacija = new Lokacija(GeoSirina, GeoDuzina, new Adresa(ulica, int.Parse(broj), int.Parse(Postanski), mesto));

            ApartmaniISadrzaji.Apartmani[ID].TipApartmana = tip;
            ApartmaniISadrzaji.Apartmani[ID].BrojGostiju = int.Parse(brojGostiju);
            ApartmaniISadrzaji.Apartmani[ID].BrojSoba = int.Parse( brojSoba);
            ApartmaniISadrzaji.Apartmani[ID].Lokacija = lokacija;
            ApartmaniISadrzaji.Apartmani[ID].Cena = int.Parse(cena);
            if (!string.IsNullOrEmpty(form["Sadrzaji"]))
            {
                ApartmaniISadrzaji.Apartmani[ID].SadrzajApartmana.Clear();
                BrisanjeStarogSadrzaja(ID, "apartmanID_SadrzajID.txt");
                string OznaceniSadrzaj = form["Sadrzaji"];
                string[] SadrzajiID = OznaceniSadrzaj.Split(',');
                foreach (string id in SadrzajiID)
                {
                    ApartmaniISadrzaji.Apartmani[ID].SadrzajApartmana.Add(ApartmaniISadrzaji.Sadrzaji[int.Parse(id)]);
                    UpisApartmanSadrzaj(ID, id, "apartmanID_SadrzajID.txt");
                }
            }
            Domacini.domacini[domacin.KorisnickoIme].ApartmaniZaIzdavanje[ID] = ApartmaniISadrzaji.Apartmani[ID];
            UpisIzmeneApartmana(ApartmaniISadrzaji.Apartmani[ID], "apartmani.txt");

            return RedirectToAction("Index");
        }

        //Ispod Su metode za rad sa TXT fajlovima

        private void UpisApartmanSadrzaj(int IDApartmana, string IDSadrzaja, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter tw = new StreamWriter(stream))
            {
                string upis = "Apartman" + IDApartmana.ToString() + "|" + IDSadrzaja;
                tw.WriteLine(upis);
            }
            stream.Close();
        }
        private void UpisApartmanSlika(int IDApartmana, int IDSlike, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter tw = new StreamWriter(stream))
            {
                string upis = IDSlike.ToString() + "|" + "Apartman" + IDApartmana.ToString();
                tw.WriteLine(upis);
            }
            stream.Close();
        }

        private void UpisApartmana(Apartman a, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter tw = new StreamWriter(stream))
            {

                string upis = a.Obrisan.ToString() + "|" + "Apartman" + a.ID.ToString() + "|" + a.TipApartmana + "|" + a.StatusApartmana + "|" + a.BrojSoba +
                              "|" + a.BrojGostiju + "|" + a.Cena + "|" + a.VremePrijave + "|" + a.VremeOdjave + "|" + a.Lokacija.Adresa.NaseljenoMesto +
                              "|" + a.Lokacija.Adresa.PostanskiBroj + "|" + a.Lokacija.Adresa.Ulica + "|" + a.Lokacija.Adresa.Broj + "|" +
                              a.Lokacija.GeoSirina + "|" + a.Lokacija.GeoDuzina + "|" + a.Domacin.KorisnickoIme;
                tw.WriteLine(upis);
            }
            stream.Close();
        }
        private void UpisSlike(Slika s, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter tw = new StreamWriter(stream))
            {
                string upis = s.ID.ToString() + "|" + s.lokalnaPutanja + "|" + s.naziv;
                tw.WriteLine(upis);
            }
            stream.Close();
        }



        private void UpisIzmeneApartmana(Apartman a, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            string[] lines = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("Apartman" + a.ID.ToString()))
                {
                    string upis = a.Obrisan.ToString() + "|" + "Apartman" + a.ID.ToString() + "|" + a.TipApartmana + "|" + a.StatusApartmana + "|" + a.BrojSoba +
                               "|" + a.BrojGostiju + "|" + a.Cena + "|" + a.VremePrijave + "|" + a.VremeOdjave + "|" + a.Lokacija.Adresa.NaseljenoMesto +
                               "|" + a.Lokacija.Adresa.PostanskiBroj + "|" + a.Lokacija.Adresa.Ulica + "|" + a.Lokacija.Adresa.Broj + "|" +
                               a.Lokacija.GeoSirina + "|" + a.Lokacija.GeoDuzina + "|" + a.Domacin.KorisnickoIme;
                    lines[i] = upis;
                }
            }
            System.IO.File.WriteAllLines(path, lines);
        }

        private void BrisanjeStarogSadrzaja(int IDApartmana, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            string[] lines = System.IO.File.ReadAllLines(path);
            List<string> listaKorisnika = lines.ToList();
            for (int i = lines.Length-1; i >= 0; i--)
            {
                if (lines[i].Contains("Apartman"+IDApartmana.ToString()))
                {
                    listaKorisnika.RemoveAt(i);
                }
            }
            System.IO.File.WriteAllLines(path, listaKorisnika.ToArray());
        }
        private void BrisanjeStarihSlika(int IDApartmana, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            string[] lines = System.IO.File.ReadAllLines(path);
            List<string> listaKorisnika = lines.ToList();
            for (int i = lines.Length-1; i >= 0; i--)
            {
                if (lines[i].Contains("Apartman" + IDApartmana.ToString()))
                {
                    listaKorisnika.RemoveAt(i);
                }
            }
            System.IO.File.WriteAllLines(path, listaKorisnika.ToArray());
        }

        private void IzmeniKorisnikaTxt(Korisnik k, string fajl, string staroIme)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            string[] lines = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains(staroIme))
                {
                    string novi = k.KorisnickoIme + '|' + k.Lozinka + '|' + k.Ime + '|' + k.Prezime + '|' + k.Pol;
                    lines[i] = novi;
                }
            }
            System.IO.File.WriteAllLines(path, lines);
        }


        private List<Apartman> sortiranje(List<Apartman> apartmani, string redosled)
        {
            if (redosled == "opadajuce")
            {
                Apartman temp = new Apartman();
                for (int i = 0; i < apartmani.Count - 1; i++)
                {
                    for (int j = i + 1; j < apartmani.Count; j++)
                    {
                        if (apartmani[i].Cena < apartmani[j].Cena)
                        {
                            temp = apartmani[j];
                            apartmani[j] = apartmani[i];
                            apartmani[i] = temp;
                        }
                    }
                }
            }
            else if (redosled == "rastuce")
            {
                Apartman temp = new Apartman();
                for (int i = 0; i < apartmani.Count - 1; i++)
                {
                    for (int j = i + 1; j < apartmani.Count; j++)
                    {
                        if (apartmani[i].Cena > apartmani[j].Cena)
                        {
                            temp = apartmani[j];
                            apartmani[j] = apartmani[i];
                            apartmani[i] = temp;
                        }
                    }
                }
            }
            return apartmani;
        }

        private List<Apartman> pretraga(List<Apartman> apartmani, string Grad, string BrojOsoba, string BrojSoba, string CenaOd, string CenaDo)
        {
            List<Apartman> apartmaniPom = new List<Apartman>();
            foreach (var apartman in apartmani)
            {
                if (!string.IsNullOrEmpty(Grad))
                {
                    if (!(apartman.Lokacija.Adresa.NaseljenoMesto == Grad))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(BrojOsoba))
                {
                    if (!(apartman.BrojGostiju == int.Parse(BrojOsoba)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(BrojSoba))
                {
                    if (!(apartman.BrojSoba == int.Parse(BrojSoba)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(CenaOd))
                {
                    if ((apartman.Cena < int.Parse(CenaOd)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(CenaDo))
                {
                    if ((apartman.Cena > int.Parse(CenaDo)))
                    {
                        continue;
                    }
                }
                apartmaniPom.Add(apartman);
            }
            return apartmaniPom;
        }

        private List<Apartman> filtrirajPoSadrzaji(List<Apartman> apartmani, string sadrzaji)
        {
            string sviID = sadrzaji;
            string[] SadrzajiID = sviID.Split(',');
            List<Apartman> apartmaniPom = new List<Apartman>();
            if (SadrzajiID.Count<string>() == 1)
            {
                foreach (var apart in apartmani)
                {
                    foreach (var sadrzaj in apart.SadrzajApartmana)
                    {
                        foreach (string ID in SadrzajiID)
                        {
                            if (sadrzaj.Naziv == ApartmaniISadrzaji.Sadrzaji[int.Parse(ID)].Naziv)
                            {
                                apartmaniPom.Add(apart);
                            }
                        }
                    }
                }
            }
            else //Oko je korisnik izabrao vise sadrzaja..pa trazimo samo apartmane koji sadrze sve to
            {
                foreach (var apart in apartmani)
                {
                    bool nasaoprethodni = true;
                    bool nadjeniSvi = true;
                    foreach (string ID in SadrzajiID)
                    {
                        if (nasaoprethodni)
                        {
                            nasaoprethodni = false;
                            foreach (var sadrzaj in apart.SadrzajApartmana)
                            {

                                if (sadrzaj.Naziv == ApartmaniISadrzaji.Sadrzaji[int.Parse(ID)].Naziv)
                                {
                                    nasaoprethodni = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            nadjeniSvi = false;
                            break;
                        }
                    }
                    if (nadjeniSvi && nasaoprethodni) //drugi uslov hvata slucaj ako zadna stavka nije nadjena
                    {
                        apartmaniPom.Add(apart);
                    }
                }
            }
            return apartmaniPom;
        }
    }
}