﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFirstMVCWebApp.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index(string KorIme, string Uloga,string Pol)
        {
            Dictionary<string, Korisnik> korisnici = new Dictionary<string, Korisnik>();
            foreach (var kor in Administratori.aministratori.Values)
            {
                korisnici.Add(kor.KorisnickoIme, kor);
            }
            foreach (var kor in Gosti.gosti.Values)
            {
                korisnici.Add(kor.KorisnickoIme, kor);
            }
            foreach (var kor in Domacini.domacini.Values)
            {
                korisnici.Add(kor.KorisnickoIme, kor);
            }
            if (Uloga != "SVI" && Uloga!=null)
            {
                Enumeracije.Uloga uloga;
                if (Uloga == "ADMIN"){
                    uloga = Enumeracije.Uloga.ADMIN;
                }
                else if (Uloga == "DOMACIN"){
                    uloga = Enumeracije.Uloga.DOMACIN;
                }
                else{
                    uloga = Enumeracije.Uloga.GOST;
                }
                Dictionary<string, Korisnik> korisniciPomocna = new Dictionary<string, Korisnik>();
                foreach (var korisnik in korisnici.Values){
                    if (korisnik.Uloga==uloga){
                        korisniciPomocna[korisnik.KorisnickoIme]=korisnik;
                    }
                }
                korisnici=korisniciPomocna;
            }
            if (Pol != "SVI" && Pol!=null)
            {
                Enumeracije.Pol pol = Enumeracije.Pol.MUSKO;
                if (Pol == "MUSKO"){
                    pol = Enumeracije.Pol.MUSKO;
                }
                else{
                    pol = Enumeracije.Pol.ZENSKO;
                }
                Dictionary<string, Korisnik> korisniciPomocna = new Dictionary<string, Korisnik>();
                foreach (var korisnik in korisnici.Values)
                {
                    if (korisnik.Pol==pol)
                    {
                        korisniciPomocna[korisnik.KorisnickoIme] = korisnik;
                    }
                }
                korisnici = korisniciPomocna;
            }
            if (!string.IsNullOrEmpty(KorIme))
            {
                Dictionary<string, Korisnik> korisniciPomocna = new Dictionary<string, Korisnik>();
                foreach (var korisnik in korisnici.Values)
                {
                    if (korisnik.KorisnickoIme==KorIme)
                    {
                        korisniciPomocna[korisnik.KorisnickoIme] = korisnik;
                    }
                }
                korisnici = korisniciPomocna;
            }


            return View(korisnici);
        }

        public ActionResult PrikazProfila()
        {
            HttpCookie cookie = Request.Cookies["Korisnik"];
            var admin = Administratori.aministratori[cookie.Value];
            return View(admin);
        }

        [HttpPost]
        public ActionResult AzuriranjeProfila(string korime, string lozinka, string ime, string prezime, string pol)
        {
            if (string.IsNullOrEmpty(korime) || string.IsNullOrEmpty(lozinka) || string.IsNullOrEmpty(ime) || string.IsNullOrEmpty(prezime) || string.IsNullOrEmpty(pol))
            {
                Session["Poruka"] = "Sva polja moraju biti popunjena.";
                return RedirectToAction("PrikazProfila");
            }
            HttpCookie cookie = Request.Cookies["Korisnik"];
            var admin = Administratori.aministratori[cookie.Value];

            Enumeracije.Pol Pol;
            if (pol == Enumeracije.Pol.MUSKO.ToString())
            {
                Pol = Enumeracije.Pol.MUSKO;
            }
            else
            {
                Pol = Enumeracije.Pol.ZENSKO;
            }

            if (admin.KorisnickoIme != korime) //ako je menjao kor ime
            {
                if (!Administratori.aministratori.ContainsKey(korime) && !Domacini.domacini.ContainsKey(korime) && !Gosti.gosti.ContainsKey(korime)) //ako novo kor ime niko drugi vec ne koristi
                {
                    string staroIme = admin.KorisnickoIme;
                    Administratori.aministratori[staroIme].KorisnickoIme = korime;
                    Administratori.aministratori[staroIme].Lozinka = lozinka;
                    Administratori.aministratori[staroIme].Ime = ime;
                    Administratori.aministratori[staroIme].Prezime = prezime;
                    Administratori.aministratori[staroIme].Pol = Pol;
                    HttpCookie AdminCookie = new HttpCookie("Korisnik", korime); //Dodeli mu i novi kolacic
                    HttpContext.Response.SetCookie(AdminCookie);
                    Administratori.aministratori[korime] = Administratori.aministratori[staroIme];
                    Administratori.aministratori.Remove(staroIme);
                    IzmeniKorisnikaTxt(Administratori.aministratori[korime], "Administratori.txt",staroIme);

                    Session["Poruka"] = "Podaci su promenjeni uspesno.";
                    return RedirectToAction("PrikazProfila");
                }
                else
                {
                    Session["Poruka"] = "Ovo korisnicko ime je zauzeto.";
                    return RedirectToAction("PrikazProfila");
                }
            }
            else //Ako nije menjao kor ime
            {
                Administratori.aministratori[korime].KorisnickoIme = korime;
                Administratori.aministratori[korime].Lozinka = lozinka;
                Administratori.aministratori[korime].Ime = ime;
                Administratori.aministratori[korime].Prezime = prezime;
                Administratori.aministratori[korime].Pol = Pol;
                IzmeniKorisnikaTxt(Administratori.aministratori[korime], "Administratori.txt",korime);

                Session["Poruka"] = "Podaci su promenjeni uspesno.";
                return RedirectToAction("PrikazProfila");
            }
        }
        
        public ActionResult DodajDomacina(string korisnickoime)
        {
            var gost = Gosti.gosti[korisnickoime];
            Domacin domacin = new Domacin(gost.KorisnickoIme,gost.Lozinka,gost.Ime,gost.Prezime,gost.Pol.ToString());
            Gosti.gosti.Remove(korisnickoime);
            Domacini.domacini.Add(korisnickoime, domacin);
            BrisanjeIzFajla(gost, "gosti.txt");
            UpisKorisnikaUFajl(domacin, "domacini.txt");
            return RedirectToAction("Index");
        }


        private MultiSelectList GetSadrzaj(string[] OznaceniSadrzaj)
        {
            List<Sadrzaj> Sadrzaji = new List<Sadrzaj>();
            foreach(var sadrzaj in ApartmaniISadrzaji.Sadrzaji.Values)
            {
                if (sadrzaj.Obrisan == false)
                {
                    Sadrzaji.Add(sadrzaj);
                }
            }

            return new MultiSelectList(Sadrzaji, "ID", "Naziv", OznaceniSadrzaj);
        }

        public ActionResult Apartmani(FormCollection form, string STATUS, string TIP, string Grad, string CenaOd, string CenaDo, string BrojOsoba, string BrojSoba)
        {
            List<Apartman> apartmani = new List<Apartman>();
            foreach(var apartman in ApartmaniISadrzaji.Apartmani.Values)
            {
                if (apartman.Obrisan == false){
                    apartmani.Add(apartman);
                }
            }

            if (Request["dugme"] == "sortiraj")
            {
                apartmani= sortiranje(apartmani, Request["SORTIRANJE"]);
            }
            else if (Request["dugme"] == "Primeni filter")
            {
                if (!string.IsNullOrEmpty(form["Sadrzaji"]))
                {
                    apartmani=filtrirajPoSadrzaji(apartmani, form["Sadrzaji"]);
                }
                if (STATUS != null && STATUS != "SVI")
                {
                    Enumeracije.StatusApartmana status;
                    if (STATUS == "AKTIVAN"){
                        status = Enumeracije.StatusApartmana.AKTIVAN;
                    }
                    else{
                        status = Enumeracije.StatusApartmana.NEAKTIVAN;
                    }
                    List<Apartman> apartmaniPom = new List<Apartman>();
                    foreach (var apart in apartmani)
                    {
                        if (apart.StatusApartmana == status){
                            apartmaniPom.Add(apart);
                        }
                    }
                    apartmani = apartmaniPom;
                }
                if (TIP != null && TIP != "SVI")
                {
                    Enumeracije.TipApart tip;
                    if (TIP == "CEO_APART"){
                        tip = Enumeracije.TipApart.CEO_APART;
                    }
                    else{
                        tip = Enumeracije.TipApart.SOBA;
                    }
                    List<Apartman> apartmaniPom = new List<Apartman>();
                    foreach (var apart in apartmani)
                    {
                        if (apart.TipApartmana == tip){
                            apartmaniPom.Add(apart);
                        }
                    }
                    apartmani = apartmaniPom;
                }
            }
            else if (Request["dugme"] == "Pretrazi")
            {
                apartmani= pretraga(apartmani, Grad, BrojOsoba, BrojSoba, CenaOd, CenaDo);
            }

            ViewBag.ListaSadrzaja = GetSadrzaj(null);
            return View(apartmani);
        }

        public ActionResult IzmeniIzbrisi(int id)
        {
            if (Request["izmena"] == "Modifikuj")
            {
                ViewBag.ListaSadrzaja = GetSadrzaj(null);
                return View(ApartmaniISadrzaji.Apartmani[id]);
            }
            else
            {
                ApartmaniISadrzaji.Apartmani[id].Obrisan = true;
                Domacin domacin = ApartmaniISadrzaji.Apartmani[id].Domacin;
                Domacini.domacini[domacin.KorisnickoIme].ApartmaniZaIzdavanje[id].Obrisan = true;
                UpisIzmeneApartmana(ApartmaniISadrzaji.Apartmani[id], "apartmani.txt");
                return RedirectToAction("Apartmani");
            }
        }

        [HttpPost]
        public ActionResult IzmenaApartmana(FormCollection form,string Status, string Postanski,string GeoSirina, string GeoDuzina, int ID, string TipApa, string brojSoba, string brojGostiju, string mesto, string ulica, string broj, string cena)
        {
            Enumeracije.TipApart tip;
            if (Enumeracije.TipApart.CEO_APART.ToString() == TipApa){
                tip = Enumeracije.TipApart.CEO_APART;
            }
            else{
                tip = Enumeracije.TipApart.SOBA;
            }
            Enumeracije.StatusApartmana status;
            if (Enumeracije.StatusApartmana.AKTIVAN.ToString() == Status){
                status = Enumeracije.StatusApartmana.AKTIVAN;
            }
            else{
                status = Enumeracije.StatusApartmana.NEAKTIVAN;
            }

            if (Request.Files.Count > 0)
            {
               bool stareIzbrisane = false;
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    if (file != null && file.ContentLength > 0)
                    {
                        if (!stareIzbrisane) //Ako su dodate nove..stare ukloni
                        {
                            BrisanjeStarihSlika(ID, "SlikaID_ApartmanID.txt");
                            ApartmaniISadrzaji.Apartmani[ID].Slike.Clear();
                            stareIzbrisane = true;
                        }
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Images/"), fileName);

                        file.SaveAs(path);
                        Slika slika = new Slika(fileName, path);
                        ApartmaniISadrzaji.Apartmani[ID].Slike.Add(slika);
                        UpisSlike(slika, "slike.txt");
                        UpisApartmanSlika(ID, slika.ID, "SlikaID_ApartmanID.txt");
                    } 
                }
            }
            
            Lokacija lokacija = new Lokacija(GeoSirina, GeoDuzina, new Adresa(ulica, int.Parse(broj), int.Parse(Postanski), mesto));
          
            ApartmaniISadrzaji.Apartmani[ID].TipApartmana = tip;
            ApartmaniISadrzaji.Apartmani[ID].BrojGostiju =int.Parse( brojGostiju);
            ApartmaniISadrzaji.Apartmani[ID].BrojSoba= int.Parse(brojSoba);
            ApartmaniISadrzaji.Apartmani[ID].Lokacija = lokacija;
            ApartmaniISadrzaji.Apartmani[ID].Cena = int.Parse(cena);
            ApartmaniISadrzaji.Apartmani[ID].StatusApartmana=status;
            if (!string.IsNullOrEmpty(form["Sadrzaji"]))
            {
                BrisanjeStarogSadrzaja(ID, "apartmanID_SadrzajID.txt");
                ApartmaniISadrzaji.Apartmani[ID].SadrzajApartmana.Clear();
                string OznaceniSadrzaj = form["Sadrzaji"];
                string[] SadrzajiID = OznaceniSadrzaj.Split(',');
                foreach (string id in SadrzajiID)
                {
                    ApartmaniISadrzaji.Apartmani[ID].SadrzajApartmana.Add(ApartmaniISadrzaji.Sadrzaji[int.Parse(id)]);
                    UpisApartmanSadrzaj(ID, id, "apartmanID_SadrzajID.txt");
                }
            }
            Domacin domacin = ApartmaniISadrzaji.Apartmani[ID].Domacin;
            Domacini.domacini[domacin.KorisnickoIme].ApartmaniZaIzdavanje[ID] = ApartmaniISadrzaji.Apartmani[ID];
            UpisIzmeneApartmana(ApartmaniISadrzaji.Apartmani[ID], "apartmani.txt");
            return RedirectToAction("Apartmani");
        }


        public ActionResult Sadrzaji()
        {
            Dictionary<int, Sadrzaj> Sadrzaji = new Dictionary<int, Sadrzaj>();
            foreach(var sadrzaj in ApartmaniISadrzaji.Sadrzaji.Values)
            {
                if (sadrzaj.Obrisan == false)
                {
                    Sadrzaji[sadrzaj.ID] = sadrzaj;
                }
            }
            return View(Sadrzaji);
        }
        public ActionResult DodavanjeSadrzaja(string naziv)
        {
            if (string.IsNullOrEmpty(naziv) || (int.TryParse(naziv,out int n)))
            {
                Session["PorukaSadrzaj"] = "Unesite ispravan naziv";
                return RedirectToAction("Sadrzaji");
            }
            Session["PorukaSadrzaj"] = "";
            var sadrzaj = new Sadrzaj(naziv);
            ApartmaniISadrzaji.Sadrzaji[sadrzaj.ID] = sadrzaj;
            UpisSadrzaja(sadrzaj, "sadrzaji.txt");
            return RedirectToAction("Sadrzaji");
        }
        
        public ActionResult IzmenaSadrzaja(int id)
        {
            if (Request["izmena"] == "Modifikuj")
            {
                return View(ApartmaniISadrzaji.Sadrzaji[id]);
               
            }
            else
            {
                ApartmaniISadrzaji.Sadrzaji[id].Obrisan = true;
                UpisIzmeneSadrzaja(ApartmaniISadrzaji.Sadrzaji[id], "sadrzaji.txt");
                foreach (var korisnik in Domacini.domacini.Values)
                {
                    foreach (var apartnam in korisnik.ApartmaniZaIzdavanje.Values)
                    {
                        foreach (var sadrzaj in apartnam.SadrzajApartmana)
                        {
                            if (sadrzaj.ID == id)
                            {
                                sadrzaj.Obrisan = true;
                            }
                        }
                    }
                }
                UpisIzmeneSadrzaja(ApartmaniISadrzaji.Sadrzaji[id], "sadrzaji.txt");
                //ApartmaniISadrzaji.Sadrzaji.Remove(id);
                return RedirectToAction("Sadrzaji");
            }
        }

        public ActionResult Modifikovanje(int id,string naziv)
        {
           
            foreach (var sadrzaj in ApartmaniISadrzaji.Sadrzaji.Values)
            {
                if (sadrzaj.ID == id)
                {
                    sadrzaj.Naziv = naziv;
                    UpisIzmeneSadrzaja(sadrzaj, "sadrzaji.txt");
                    break;
                }
            }
            return RedirectToAction("Sadrzaji");
        }


        //Ispod su Metode za rad sa TXT fajlovima kao i metode za sortiranje i pretrazivanje

        private void UpisApartmanSadrzaj(int IDApartmana, string IDSadrzaja, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter tw = new StreamWriter(stream))
            {
                string upis = "Apartman" + IDApartmana.ToString() + "|" + IDSadrzaja;
                tw.WriteLine(upis);
            }
            stream.Close();
        }

        private void UpisSadrzaja(Sadrzaj s, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter tw = new StreamWriter(stream))
            {
                string upis = s.ID.ToString() + "|" + s.Naziv + "|" + s.Obrisan.ToString();
                tw.WriteLine(upis);
            }
            stream.Close();
        }

        private void UpisIzmeneSadrzaja(Sadrzaj s, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            string[] lines = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains(s.ID.ToString()))
                {
                    string upis = s.ID.ToString() + "|" + s.Naziv + "|" + s.Obrisan.ToString();
                    lines[i] = upis;
                }
            }
            System.IO.File.WriteAllLines(path, lines);
        }

        private void IzmeniKorisnikaTxt(Korisnik k, string fajl, string staroIme)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            string[] lines = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains(staroIme))
                {
                    string novi = k.KorisnickoIme + '|' + k.Lozinka + '|' + k.Ime + '|' + k.Prezime + '|' + k.Pol + "|" + k.Uloga;
                    lines[i] = novi;
                }
            }
            System.IO.File.WriteAllLines(path, lines);
        }

        private void BrisanjeIzFajla(Korisnik k, string fajl) //Za brisanje gosta,kada se proglasi kao domacin
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            string[] lines = System.IO.File.ReadAllLines(path);
            List<string> listaKorisnika = lines.ToList();
            for (int i = lines.Length - 1; i >= 0; i--)
            {
                if (lines[i].Contains(k.KorisnickoIme))
                {
                    listaKorisnika.RemoveAt(i);
                }
            }
            System.IO.File.WriteAllLines(path, listaKorisnika.ToArray());
        }

        private void UpisKorisnikaUFajl(Korisnik k, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter tw = new StreamWriter(stream))
            {
                string upis = k.KorisnickoIme + '|' + k.Lozinka + '|' + k.Ime + '|' + k.Prezime + '|' + k.Pol;
                tw.WriteLine(upis);
            }
            stream.Close();
        }

        private void UpisIzmeneApartmana(Apartman a, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            string[] lines = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("Apartman" + a.ID.ToString())) //ovakav uslov da bi znao sta je id
                {
                    string upis = a.Obrisan.ToString() + "|" + "Apartman" + a.ID.ToString() + "|" + a.TipApartmana + "|" + a.StatusApartmana + "|" + a.BrojSoba +
                               "|" + a.BrojGostiju + "|" + a.Cena + "|" + a.VremePrijave + "|" + a.VremeOdjave + "|" + a.Lokacija.Adresa.NaseljenoMesto +
                               "|" + a.Lokacija.Adresa.PostanskiBroj + "|" + a.Lokacija.Adresa.Ulica + "|" + a.Lokacija.Adresa.Broj + "|" +
                               a.Lokacija.GeoSirina + "|" + a.Lokacija.GeoDuzina + "|" + a.Domacin.KorisnickoIme;
                    lines[i] = upis;
                }
            }
            System.IO.File.WriteAllLines(path, lines);
        }

        private void BrisanjeStarogSadrzaja(int IDApartmana, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            string[] lines = System.IO.File.ReadAllLines(path);
            List<string> listaKorisnika = lines.ToList();
            for (int i = lines.Length - 1; i >= 0; i--)
            {
                if (lines[i].Contains("Apartman" + IDApartmana.ToString()))
                {
                    listaKorisnika.RemoveAt(i);
                }
            }
            System.IO.File.WriteAllLines(path, listaKorisnika.ToArray());
        }
        private void UpisApartmanSlika(int IDApartmana, int IDSlike, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter tw = new StreamWriter(stream))
            {
                string upis = IDSlike.ToString() + "|" + "Apartman" + IDApartmana.ToString();
                tw.WriteLine(upis);
            }
            stream.Close();
        }

        private void BrisanjeStarihSlika(int IDApartmana, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            string[] lines = System.IO.File.ReadAllLines(path);
            List<string> listaKorisnika = lines.ToList();
            for (int i = lines.Length - 1; i >= 0; i--)
            {
                if (lines[i].Contains("Apartman" + IDApartmana.ToString()))
                {
                    listaKorisnika.RemoveAt(i);
                }
            }
            System.IO.File.WriteAllLines(path, listaKorisnika.ToArray());
        }
        private void UpisSlike(Slika s, string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/" + fajl);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter tw = new StreamWriter(stream))
            {
                string upis = s.ID.ToString() + "|" + s.lokalnaPutanja + "|" + s.naziv;
                tw.WriteLine(upis);
            }
            stream.Close();
        }

        private List<Apartman> sortiranje(List<Apartman> apartmani, string redosled )
        {
            if (redosled == "opadajuce")
            {
                Apartman temp = new Apartman();
                for (int i = 0; i < apartmani.Count - 1; i++)
                {
                    for (int j = i + 1; j < apartmani.Count; j++)
                    {
                        if (apartmani[i].Cena < apartmani[j].Cena)
                        {
                            temp = apartmani[j];
                            apartmani[j] = apartmani[i];
                            apartmani[i] = temp;
                        }
                    }
                }
            }
            else if (redosled == "rastuce")
            {
                Apartman temp = new Apartman();
                for (int i = 0; i < apartmani.Count - 1; i++)
                {
                    for (int j = i + 1; j < apartmani.Count; j++)
                    {
                        if (apartmani[i].Cena > apartmani[j].Cena)
                        {
                            temp = apartmani[j];
                            apartmani[j] = apartmani[i];
                            apartmani[i] = temp;
                        }
                    }
                }
            }
            return apartmani;
        }

        private List<Apartman> pretraga(List<Apartman> apartmani,string Grad,string BrojOsoba,string BrojSoba,string CenaOd,string CenaDo)
        {
            List<Apartman> apartmaniPom = new List<Apartman>();
            foreach (var apartman in apartmani)
            {
                if (!string.IsNullOrEmpty(Grad))
                {
                    if (!(apartman.Lokacija.Adresa.NaseljenoMesto == Grad))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(BrojOsoba))
                {
                    if (!(apartman.BrojGostiju == int.Parse(BrojOsoba)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(BrojSoba))
                {
                    if (!(apartman.BrojSoba == int.Parse(BrojSoba)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(CenaOd))
                {
                    if ((apartman.Cena < int.Parse(CenaOd)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(CenaDo))
                {
                    if ((apartman.Cena > int.Parse(CenaDo)))
                    {
                        continue;
                    }
                }
                apartmaniPom.Add(apartman);
            }
            return apartmaniPom;
        }

        private List<Apartman> filtrirajPoSadrzaji(List<Apartman> apartmani, string sadrzaji)
        {
            string sviID = sadrzaji;
            string[] SadrzajiID = sviID.Split(',');
            List<Apartman> apartmaniPom = new List<Apartman>();
            if (SadrzajiID.Count<string>() == 1)
            {
                foreach (var apart in apartmani)
                {
                    foreach (var sadrzaj in apart.SadrzajApartmana)
                    {
                        foreach (string ID in SadrzajiID)
                        {
                            if (sadrzaj.Naziv == ApartmaniISadrzaji.Sadrzaji[int.Parse(ID)].Naziv)
                            {
                                apartmaniPom.Add(apart);
                            }
                        }
                    }
                }
            }
            else //Oko je korisnik izabrao vise sadrzaja..pa trazimo samo apartmane koji sadrze sve to
            {
                foreach (var apart in apartmani)
                {
                    bool nasaoprethodni = true;
                    bool nadjeniSvi = true;
                    foreach (string ID in SadrzajiID)
                    {
                        if (nasaoprethodni)
                        {
                            nasaoprethodni = false;
                            foreach (var sadrzaj in apart.SadrzajApartmana)
                            {

                                if (sadrzaj.Naziv == ApartmaniISadrzaji.Sadrzaji[int.Parse(ID)].Naziv)
                                {
                                    nasaoprethodni = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            nadjeniSvi = false;
                            break;
                        }
                    }
                    if (nadjeniSvi && nasaoprethodni) //drugi uslov hvata slucaj ako zadna stavka nije nadjena
                    {
                        apartmaniPom.Add(apart);
                    }
                }
            }
            return apartmaniPom;
        }
        
    }
}