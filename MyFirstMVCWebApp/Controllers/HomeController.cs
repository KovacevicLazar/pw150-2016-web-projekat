﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace MyFirstMVCWebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(FormCollection form, string TIP, string Grad, string CenaOd, string CenaDo, string BrojOsoba, string BrojSoba)
        {
            
            //ViewBag.sviKorisnici = sviKorisnici;

            List<Apartman> apartmani = new List<Apartman>();
            foreach (var apart in ApartmaniISadrzaji.Apartmani.Values)
            {
                if (apart.StatusApartmana == Enumeracije.StatusApartmana.AKTIVAN && apart.Obrisan == false)
                {
                    apartmani.Add(apart);
                }
            }

            if (Request["dugme"] == "sortiraj")
            {
                apartmani = sortiranje(apartmani, Request["SORTIRANJE"]);
            }
            else if (Request["dugme"] == "Primeni filter")
            {
                if (!string.IsNullOrEmpty(form["Sadrzaji"]))
                {
                    apartmani = filtrirajPoSadrzaji(apartmani, form["Sadrzaji"]);
                }
               
                if (TIP != null && TIP != "SVI")
                {
                    Enumeracije.TipApart tip;
                    if (TIP == "CEO_APART")
                    {
                        tip = Enumeracije.TipApart.CEO_APART;
                    }
                    else
                    {
                        tip = Enumeracije.TipApart.SOBA;
                    }
                    List<Apartman> apartmaniPom = new List<Apartman>();
                    foreach (var apart in apartmani)
                    {
                        if (apart.TipApartmana == tip)
                        {
                            apartmaniPom.Add(apart);
                        }
                    }
                    apartmani = apartmaniPom;
                }
            }
            else if (Request["dugme"] == "Pretrazi")
            {
                apartmani = pretraga(apartmani, Grad, BrojOsoba, BrojSoba, CenaOd, CenaDo);
            }

            ViewBag.ListaSadrzaja = GetSadrzaj(null);
            return View(apartmani);
        }


        private MultiSelectList GetSadrzaj(string[] OznaceniSadrzaj)
        {
            List<Sadrzaj> Sadrzaji = new List<Sadrzaj>();
            foreach (var sadrzaj in ApartmaniISadrzaji.Sadrzaji.Values)
            {
                if (sadrzaj.Obrisan == false)
                {
                    Sadrzaji.Add(sadrzaj);
                }
            }
            return new MultiSelectList(Sadrzaji, "ID", "Naziv", OznaceniSadrzaj);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        private List<Apartman> sortiranje(List<Apartman> apartmani, string redosled)
        {
            if (redosled == "opadajuce")
            {
                Apartman temp = new Apartman();
                for (int i = 0; i < apartmani.Count - 1; i++)
                {
                    for (int j = i + 1; j < apartmani.Count; j++)
                    {
                        if (apartmani[i].Cena < apartmani[j].Cena)
                        {
                            temp = apartmani[j];
                            apartmani[j] = apartmani[i];
                            apartmani[i] = temp;
                        }
                    }
                }
            }
            else if (redosled == "rastuce")
            {
                Apartman temp = new Apartman();
                for (int i = 0; i < apartmani.Count - 1; i++)
                {
                    for (int j = i + 1; j < apartmani.Count; j++)
                    {
                        if (apartmani[i].Cena > apartmani[j].Cena)
                        {
                            temp = apartmani[j];
                            apartmani[j] = apartmani[i];
                            apartmani[i] = temp;
                        }
                    }
                }
            }
            return apartmani;
        }

        private List<Apartman> pretraga(List<Apartman> apartmani, string Grad, string BrojOsoba, string BrojSoba, string CenaOd, string CenaDo)
        {
            List<Apartman> apartmaniPom = new List<Apartman>();
            foreach (var apartman in apartmani)
            {
                if (!string.IsNullOrEmpty(Grad))
                {
                    if (!(apartman.Lokacija.Adresa.NaseljenoMesto == Grad))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(BrojOsoba))
                {
                    if (!(apartman.BrojGostiju == int.Parse(BrojOsoba)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(BrojSoba))
                {
                    if (!(apartman.BrojSoba == int.Parse(BrojSoba)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(CenaOd))
                {
                    if ((apartman.Cena < int.Parse(CenaOd)))
                    {
                        continue;
                    }
                }
                if (!string.IsNullOrEmpty(CenaDo))
                {
                    if ((apartman.Cena > int.Parse(CenaDo)))
                    {
                        continue;
                    }
                }
                apartmaniPom.Add(apartman);
            }
            return apartmaniPom;
        }

        private List<Apartman> filtrirajPoSadrzaji(List<Apartman> apartmani, string sadrzaji)
        {
            string sviID = sadrzaji;
            string[] SadrzajiID = sviID.Split(',');
            List<Apartman> apartmaniPom = new List<Apartman>();
            if (SadrzajiID.Count<string>() == 1)
            {
                foreach (var apart in apartmani)
                {
                    foreach (var sadrzaj in apart.SadrzajApartmana)
                    {
                        foreach (string ID in SadrzajiID)
                        {
                            if (sadrzaj.Naziv == ApartmaniISadrzaji.Sadrzaji[int.Parse(ID)].Naziv)
                            {
                                apartmaniPom.Add(apart);
                            }
                        }
                    }
                }
            }
            else //Oko je korisnik izabrao vise sadrzaja..pa trazimo samo apartmane koji sadrze sve to
            {
                foreach (var apart in apartmani)
                {
                    bool nasaoprethodni = true;
                    bool nadjeniSvi = true;
                    foreach (string ID in SadrzajiID)
                    {
                        if (nasaoprethodni)
                        {
                            nasaoprethodni = false;
                            foreach (var sadrzaj in apart.SadrzajApartmana)
                            {

                                if (sadrzaj.Naziv == ApartmaniISadrzaji.Sadrzaji[int.Parse(ID)].Naziv)
                                {
                                    nasaoprethodni = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            nadjeniSvi = false;
                            break;
                        }
                    }
                    if (nadjeniSvi && nasaoprethodni) //drugi uslov hvata slucaj ako zadna stavka nije nadjena
                    {
                        apartmaniPom.Add(apart);
                    }
                }
            }
            return apartmaniPom;
        }
    }
}