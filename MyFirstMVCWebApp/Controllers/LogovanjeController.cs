﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFirstMVCWebApp.Controllers
{
    public class LogovanjeController : Controller
    {
        // GET: Logovanje
        public ActionResult UlogujSe(string KorisnickoIme, string Lozinka)
        {
            Session["PorukaLogovanje"] = "Korisnik sa ovim imenom nije registrovan";
            var korisnik=new Korisnik();
            if (Administratori.aministratori.ContainsKey(KorisnickoIme))
            {
                foreach(var kor in Administratori.aministratori.Values)
                {
                    if (kor.KorisnickoIme == KorisnickoIme)
                    {
                        Session["PorukaLogovanje"] = "";
                        if (Lozinka == kor.Lozinka)
                        {
                            korisnik = kor;
                            HttpCookie AdminCookie = new HttpCookie("Korisnik", KorisnickoIme);
                            HttpContext.Response.SetCookie(AdminCookie);
                            return RedirectToAction(nameof(AdminController.Index), "Admin");
                        }
                        else
                        {
                            Session["PorukaLogovanje"] = "Nije dobro uneta Lozinka.";
                        }
                      
                    }  
                }
            }
            else if (Domacini.domacini.ContainsKey(KorisnickoIme))
            {
                foreach (var kor in Domacini.domacini.Values)
                {
                    if (kor.KorisnickoIme == KorisnickoIme)
                    {
                        Session["PorukaLogovanje"] = "";
                        if (Lozinka == kor.Lozinka)
                        {
                            korisnik = kor;
                            HttpCookie DomacinCookie = new HttpCookie("Korisnik", KorisnickoIme);
                            HttpContext.Response.SetCookie(DomacinCookie);
                            return RedirectToAction(nameof(DomacinController.Index), "Domacin");
                        }
                        else
                        {
                            Session["PorukaLogovanje"] = "Nije dobro uneta Lozinka.";
                        }
                    }
                }
            }
            else if (Gosti.gosti.ContainsKey(KorisnickoIme))
            {
                foreach (var kor in Gosti.gosti.Values)
                {
                    if (kor.KorisnickoIme == KorisnickoIme)
                    {
                        Session["PorukaLogovanje"] = "";
                        if (Lozinka == kor.Lozinka)
                        {
                            korisnik = kor;
                            HttpCookie GostCookie = new HttpCookie("Korisnik", KorisnickoIme);
                            HttpContext.Response.SetCookie(GostCookie);
                            return RedirectToAction(nameof(GostController.Index), "Gost");
                        }
                        else
                        {
                            Session["PorukaLogovanje"] = "Nije dobro uneta Lozinka.";
                        }
                    }
                }
            }
            return RedirectToAction(nameof(HomeController.Index), "Home");
            

        }
        public ActionResult Registracija()
        {
            return View();
        }

       
        public ActionResult RegistrationHandle(string KorIme,string Lozinka,string Ime,string Prezime,string Pol)
        {
            if (string.IsNullOrEmpty(KorIme) || string.IsNullOrEmpty(Lozinka) || string.IsNullOrEmpty(Ime) || string.IsNullOrEmpty(Prezime) || string.IsNullOrEmpty(Pol))
            {
                Session["PorukaRegistracija"] = "Sva polja moraju biti popunjena.";
                return RedirectToAction("Registracija");
            }

            if (!Administratori.aministratori.ContainsKey(KorIme) && !Domacini.domacini.ContainsKey(KorIme) && !Gosti.gosti.ContainsKey(KorIme))
            {
                Session["PorukaRegistracija"] = "";
                Gost gost = new Gost(KorIme, Lozinka, Ime, Prezime, Pol);
                HttpCookie GostCookie = new HttpCookie("Korisnik", gost.KorisnickoIme);
                HttpContext.Response.SetCookie(GostCookie);
                Gosti.gosti.Add(gost.KorisnickoIme, gost);
                UpisUFajl(gost, "gosti.txt");
                return RedirectToAction(nameof(GostController.Index), "Gost");
            }
            else
            {
                return View();
            }
        }
        private void UpisUFajl(Korisnik k,string fajl)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/"+fajl);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter tw = new StreamWriter(stream))
            {
                string upis = k.KorisnickoIme + '|' + k.Lozinka + '|' + k.Ime + '|' + k.Prezime + '|' + k.Pol;
                tw.WriteLine(upis);
            }
            stream.Close();
        }



    }
}