﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace MyFirstMVCWebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            Administratori admini = new Administratori("~/App_Data/Administratori.txt");
            Domacini domaci = new Domacini("~/App_Data/domacini.txt");
            Gosti gosti= new Gosti("~/App_Data/gosti.txt");
            Dictionary<int, Apartman> Apartmani = new Dictionary<int, Apartman>();
            Apartmani = ucitajApartmane("~/App_Data/apartmani.txt");
            ApartmaniISadrzaji.Apartmani = Apartmani;

            Dictionary<int, Slika> slike = ucitajSlike("~/App_Data/slike.txt");
            Dictionary<int, Sadrzaj> sadrzaji = ucitajSadrzaj("~/App_Data/sadrzaji.txt");

            
            poveziSlikuIApartman("~/App_Data/SlikaID_ApartmanID.txt",slike);
            poveziSadrzajIApartman("~/App_Data/apartmanID_SadrzajID.txt", sadrzaji);

            UcitajKomentare("~/App_Data/komentari.txt");


        }

        private Dictionary<int, Sadrzaj> ucitajSadrzaj(string path)
        {
            Dictionary<int, Sadrzaj> ret = new Dictionary<int, Sadrzaj>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while (!string.IsNullOrEmpty(line = sr.ReadLine()))
            {
                string[] tokens = line.Split('|');
                Sadrzaj sadrzaj = new Sadrzaj(tokens[2] == "True", int.Parse(tokens[0]), tokens[1]);
                ApartmaniISadrzaji.Sadrzaji.Add(sadrzaj.ID, sadrzaj);
                ret.Add(sadrzaj.ID, sadrzaj);
            }
            sr.Close();
            stream.Close();

            return ret;
        }

        private void poveziSadrzajIApartman(string path, Dictionary<int, Sadrzaj> sadrzaji)
        {
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while (!string.IsNullOrEmpty(line = sr.ReadLine()))
            {
                string[] tokens = line.Split('|');
                ApartmaniISadrzaji.Apartmani[int.Parse(tokens[0].Substring(8))].SadrzajApartmana.Add(sadrzaji[int.Parse(tokens[1])]);
            }
            sr.Close();
            stream.Close();
        }

        private void poveziSlikuIApartman(string path, Dictionary<int, Slika> slike)
        {
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while (!string.IsNullOrEmpty(line = sr.ReadLine()))
            {
                string[] tokens = line.Split('|');
                ApartmaniISadrzaji.Apartmani[int.Parse(tokens[1].Substring(8))].Slike.Add(slike[int.Parse(tokens[0])]);
            }
            sr.Close();
            stream.Close();

        }

        private void UcitajKomentare(string path)
        {
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while (!string.IsNullOrEmpty(line = sr.ReadLine()))
            {
                string[] tokens = line.Split('|');
                Komentar komentar = new Komentar(tokens[0],int.Parse( tokens[1]));
                komentar.Apartman = ApartmaniISadrzaji.Apartmani[int.Parse(tokens[2])];
                komentar.Gost = Gosti.gosti[tokens[3]];
                ApartmaniISadrzaji.Apartmani[int.Parse(tokens[2])].Komentari.Add(komentar);
            }
            sr.Close();
            stream.Close();

        }
        private Dictionary<int,Slika> ucitajSlike(string path)
        {
            Dictionary<int, Slika> Slike = new Dictionary<int, Slika> ();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while (!string.IsNullOrEmpty(line = sr.ReadLine()))
            {
                string[] tokens = line.Split('|');
                Slika slika = new Slika(int.Parse(tokens[0]),tokens[2], tokens[1]);
                Slike.Add(slika.ID,slika);
            }
            sr.Close();
            stream.Close();

            return Slike;
        }
        private Dictionary<int,Apartman> ucitajApartmane(string path)
        {
            Dictionary<int, Apartman> ret = new Dictionary<int, Apartman>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while (!string.IsNullOrEmpty(line = sr.ReadLine()))
            {
                string[] tokens = line.Split('|');
                Lokacija lokacija = new Lokacija(new Adresa(tokens[11], int.Parse(tokens[12]), int.Parse(tokens[10]), tokens[9]));
                lokacija.GeoSirina = tokens[13];
                lokacija.GeoDuzina = tokens[14];
                Enumeracije.TipApart tip;
                if(tokens[2]== "CEO_APART")
                {
                    tip = Enumeracije.TipApart.CEO_APART;
                }
                else
                    tip = Enumeracije.TipApart.SOBA;
                Enumeracije.StatusApartmana status;
                if (tokens[3] == "AKTIVAN")
                {
                    status=Enumeracije.StatusApartmana.AKTIVAN;
                }
                else
                    status = Enumeracije.StatusApartmana.NEAKTIVAN;
                Apartman apartman = new Apartman(tokens[0] == "True", int.Parse(tokens[1].Substring(8)), tip, status, int.Parse(tokens[4]), int.Parse(tokens[5]), int.Parse(tokens[6]), int.Parse(tokens[7]), int.Parse(tokens[8]), lokacija);
                apartman.Domacin = Domacini.domacini[tokens[15]];
                Domacini.domacini[tokens[15]].ApartmaniZaIzdavanje.Add(apartman.ID,apartman);
                ret[apartman.ID] = apartman;
            }
            sr.Close();
            stream.Close();
            return ret;
        }
    }
}
