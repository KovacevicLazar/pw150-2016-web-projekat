﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class Gost : Korisnik
    {
        Dictionary<int, Apartman> iznajmljeniApartmani = new Dictionary<int, Apartman>();
        Dictionary<int, Rezervacija> listaRezervacija = new Dictionary<int, Rezervacija>();

    public Gost(string KorIme,string Lozinka,string Ime,string Prezime,string Pol)
    {
        base.KorisnickoIme = KorIme;
        base.Ime = Ime;
        base.Lozinka = Lozinka;
        base.Prezime = Prezime;
        if (Pol == Enumeracije.Pol.MUSKO.ToString())
        {
            base.Pol = Enumeracije.Pol.MUSKO;
        }
        else
            base.Pol=Enumeracije.Pol.ZENSKO;
        base.Uloga = Enumeracije.Uloga.GOST;
    }
}
