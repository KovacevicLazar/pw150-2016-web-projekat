﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;

    public class Adminstrator : Korisnik
    {
        public Adminstrator(string KorisnickoIme,string Lozinka ,string Ime ,string Prezime ,string Pol,string Uloga)
        {
            base.Ime = Ime;
            base.KorisnickoIme = KorisnickoIme;
            base.Lozinka = Lozinka;
            base.Prezime = Prezime;
            if (Pol == Enumeracije.Pol.MUSKO.ToString())
            {
                base.Pol = Enumeracije.Pol.MUSKO;
            }
            else
                base.Pol = Enumeracije.Pol.ZENSKO;
            if (Uloga == Enumeracije.Uloga.ADMIN.ToString())
            {
                base.Uloga = Enumeracije.Uloga.ADMIN;
            }
        }

         
    }
