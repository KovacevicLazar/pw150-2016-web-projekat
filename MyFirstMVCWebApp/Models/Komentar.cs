﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class Komentar
    {
    public Komentar(string takstKomentara, int ocena)
    {
        TakstKomentara = takstKomentara;
        Ocena = ocena;
    }

    public Gost Gost { get; set; }
        public Apartman Apartman { get; set; }
        public string TakstKomentara { get; set; }
        public int Ocena { get; set; }
    
    }
