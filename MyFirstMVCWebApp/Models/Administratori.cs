﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;


    public class Administratori
    {
        
        public static Dictionary<string, Adminstrator> aministratori = new Dictionary<string, Adminstrator>();



        public Administratori(string path)
        {
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while (!string.IsNullOrEmpty(line = sr.ReadLine()))
            {
                string[] tokens = line.Split('|');
                Adminstrator p = new Adminstrator(tokens[0], tokens[1], tokens[2],tokens[3],tokens[4],tokens[5]);
                aministratori.Add(p.KorisnickoIme, p);
            }
            sr.Close();
            stream.Close();
         }
    }
