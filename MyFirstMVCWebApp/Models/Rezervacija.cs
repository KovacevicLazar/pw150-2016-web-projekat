﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class Rezervacija
    {
        public Apartman RezervisaniApartman { get; set; }

        public DateTime PocetniDatum { get; set; }
        public int BrojNocenja { get; set;}
        public double UkupnaCena { get; set; }

        public Gost Gost { get; set; }
        public Enumeracije.Status Status { get; set; }

    }
