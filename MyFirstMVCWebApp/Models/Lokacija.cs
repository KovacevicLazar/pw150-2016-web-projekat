﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class Lokacija
    {
    public Lokacija(Adresa adresa)
    {
        Adresa = adresa;
    }

    public Lokacija(string geoSirina, string geoDuzina, Adresa adresa)
    {
        GeoSirina = geoSirina;
        GeoDuzina = geoDuzina;
        Adresa = adresa;
    }

    public string GeoSirina { get; set; }
        public string GeoDuzina { get; set; }
        public Adresa Adresa { get; set; }
    }
