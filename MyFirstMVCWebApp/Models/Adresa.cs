﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class Adresa
    {
    public Adresa(string ulica, int broj, string naseljenoMesto)
    {
        Ulica = ulica;
        Broj = broj;
        NaseljenoMesto = naseljenoMesto;
    }

    public Adresa(string ulica, int broj, int postanskiBroj, string naseljenoMesto)
    {
        Ulica = ulica;
        Broj = broj;
        PostanskiBroj = postanskiBroj;
        NaseljenoMesto = naseljenoMesto;
    }

    public string Ulica { get; set; }
        public int Broj { get; set; }

        public int PostanskiBroj { get; set; }
        public string NaseljenoMesto { get; set; }

    public override string ToString()
    {
        return NaseljenoMesto+", "+Ulica+", "+Broj.ToString();
    }
}
