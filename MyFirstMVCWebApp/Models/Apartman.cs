﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class Apartman
    {
        public static int cntapart=0;

        public bool Obrisan { get; set; }
        public int ID { get; set; }
        public Enumeracije.TipApart TipApartmana { get; set; }
        public Enumeracije.StatusApartmana StatusApartmana { get; set; }
        public int BrojSoba { get; set; }
        public int BrojGostiju { get; set; }
        public Lokacija Lokacija {get;set;}
        public List<DateTime> DatumizaIzdavanje { get => datumi; set => datumi = value; }
        public List<DateTime> DostupniDatumi { get => dostupniDatumi; set => dostupniDatumi = value; }

        private List<DateTime> datumi = new List<DateTime>();

        private List<DateTime> dostupniDatumi = new List<DateTime>();

        public Domacin Domacin { get; set; }
       
      
        public double Cena { get; set; }

        public int VremePrijave { get; set; }

        public int VremeOdjave { get; set; }

        private List<Slika> slike = new List<Slika>();

        public List<Rezervacija> ListaRezervacija { get => listaRezervacija; set => listaRezervacija = value; }
        public List<Sadrzaj> SadrzajApartmana { get => sadrzajApartmana; set => sadrzajApartmana = value; }

        public List<Komentar> Komentari { get => komentari; set => komentari = value; }
        public List<Slika> Slike { get => slike; set => slike = value; }

         private List<Komentar> komentari = new List<Komentar>();
        private List<Sadrzaj> sadrzajApartmana = new List<Sadrzaj>();

        private List<Rezervacija> listaRezervacija = new List<Rezervacija>();

    public Apartman(Domacin domacin, Enumeracije.TipApart tipApartmana, int brojSoba, int brojGostiju, Lokacija lokacija, double cena)
    {
        ID = cntapart++;
        Domacin = domacin;
        TipApartmana = tipApartmana;
        BrojSoba = brojSoba;
        BrojGostiju = brojGostiju;
        Lokacija = lokacija;
        Cena = cena;
        StatusApartmana = Enumeracije.StatusApartmana.NEAKTIVAN;
        Obrisan = false;
        VremePrijave = 2;
        VremeOdjave = 10;
    }

    public Apartman()
    {
    }

    public Apartman(bool obrisan, int iD, Enumeracije.TipApart tipApartmana, Enumeracije.StatusApartmana statusApartmana, int brojSoba, int brojGostiju,  double cena, int vremePrijave, int vremeOdjave, Lokacija lokacija)
    {
        cntapart++;
        Obrisan = obrisan;
        ID = iD;
        TipApartmana = tipApartmana;
        StatusApartmana = statusApartmana;
        BrojSoba = brojSoba;
        BrojGostiju = brojGostiju;
        Lokacija = lokacija;
        Cena = cena;
        VremePrijave = vremePrijave;
        VremeOdjave = vremeOdjave;
    }
}
