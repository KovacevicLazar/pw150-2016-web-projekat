﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class Domacin : Korisnik
    {
        Dictionary<int, Apartman> apartmaniZaIzdavanje = new Dictionary<int, Apartman>();

        public Domacin(string KorisnickoIme, string Lozinka, string Ime, string Prezime, string Pol)
        {
            base.Ime = Ime;
            base.KorisnickoIme = KorisnickoIme;
            base.Lozinka = Lozinka;
            base.Prezime = Prezime;
            if (Pol == Enumeracije.Pol.MUSKO.ToString())
            {
                base.Pol = Enumeracije.Pol.MUSKO;
            }
            else
                base.Pol = Enumeracije.Pol.ZENSKO;
            base.Uloga = Enumeracije.Uloga.DOMACIN;
        }

    public Dictionary<int, Apartman> ApartmaniZaIzdavanje { get => apartmaniZaIzdavanje; set => apartmaniZaIzdavanje = value; }
}
