﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

public class Domacini
    {
        public static Dictionary<string, Domacin> domacini = new Dictionary<string, Domacin>();

        public Domacini(string path)
        {
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while (!string.IsNullOrEmpty(line = sr.ReadLine()))
            {
                string[] tokens = line.Split('|');
                Domacin p = new Domacin(tokens[0], tokens[1], tokens[2],tokens[3],tokens[4]);
                
                domacini.Add(p.KorisnickoIme, p);
            }
            sr.Close();
            stream.Close();
        }
    }
