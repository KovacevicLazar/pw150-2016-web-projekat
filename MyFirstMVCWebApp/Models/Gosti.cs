﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

public class Gosti
    {
        public static Dictionary<string, Gost> gosti = new Dictionary<string, Gost>();

        public Gosti(string path)
        {
        path = HostingEnvironment.MapPath(path);
        FileStream stream = new FileStream(path, FileMode.Open);
        StreamReader sr = new StreamReader(stream);
        string line = "";
        while (!string.IsNullOrEmpty(line = sr.ReadLine()))
        {
            string[] tokens = line.Split('|');
            Gost p = new Gost(tokens[0], tokens[1], tokens[2], tokens[3], tokens[4]);
            gosti.Add(p.KorisnickoIme, p);
        }
        sr.Close();
        stream.Close();
    }
    }
