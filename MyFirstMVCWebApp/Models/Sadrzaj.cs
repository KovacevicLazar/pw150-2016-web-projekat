﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

    
    public class Sadrzaj
    {
    public static int cntSadrzaj=0;
    public bool Obrisan { get; set; }
    public Sadrzaj(string naziv)
    {
        ID = cntSadrzaj++;
        Naziv = naziv;
        Obrisan = false;
    }

    public Sadrzaj(bool obrisan, int iD, string naziv)
    {
        cntSadrzaj++;
        Obrisan = obrisan;
        ID = iD;
        Naziv = naziv;
    }

    public int ID { get; set; }
    public string Naziv { get; set; }
    }
