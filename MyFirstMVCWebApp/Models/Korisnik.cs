﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class Korisnik
    {
        public string KorisnickoIme { get; set; }

        public string Lozinka { get; set; }

        public string Ime { get; set; }

        public string Prezime { get; set; }

        public Enumeracije.Pol Pol { get; set; }

        public Enumeracije.Uloga Uloga { get; set; }

    }
