﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class Enumeracije
    {
        public enum Pol : int
        {
            MUSKO = 0,
            ZENSKO,
        }
        public enum Uloga : int
        {
            ADMIN = 0,
            GOST,
            DOMACIN,
        }

        public enum TipApart : int
        {
            CEO_APART= 0,
            SOBA,
        }

        public enum Status : int
        {
            Kreirana=0,
            Odbijena,
            Odustanak,
            Prihvacena,
            Zavrsena,
           
        }

    public enum StatusApartmana : int
    {
        NEAKTIVAN = 0,
        AKTIVAN,

    }
}
