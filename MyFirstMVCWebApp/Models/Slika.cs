﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class Slika
    {
        public static int cntSlika=0;
        public int ID { get; set; }
        public string naziv { get; set; }
        public string lokalnaPutanja { get; set; }

        public Slika(string naziv, string lokalnaPutanja)
        {
            this.ID = cntSlika++;
            this.naziv = naziv;
            this.lokalnaPutanja = lokalnaPutanja;
        }

        public Slika(int iD, string naziv, string lokalnaPutanja)
        {
            cntSlika++;
            ID = iD;
            this.naziv = naziv;
            this.lokalnaPutanja = lokalnaPutanja;
        }
}
